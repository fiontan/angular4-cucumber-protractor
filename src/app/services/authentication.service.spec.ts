import { TestBed, async, inject } from '@angular/core/testing';
import {
    HttpModule,
    Http,
    Response,
    ResponseOptions,
    XHRBackend
} from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { Authentication } from './authentication.service';

describe('Service: Authentication', () => {

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpModule],
            providers:
            [
                Authentication,
                { provide: XHRBackend, useClass: MockBackend }
            ]
        });
    });

    describe('Basic tests', () => {
        it('should create Authentication service with default value',
            inject([Authentication], (Authentication: Authentication) => {
                expect(Authentication).toBeTruthy();
                expect(Authentication.authenticated).toBeFalsy();
        }));

        it('should update the authenticated property correctly',
            inject([Authentication], (Authentication: Authentication) => {
                Authentication.authenticated = true;
                expect(Authentication.authenticated).toBeTruthy();
        }));
    });

});
