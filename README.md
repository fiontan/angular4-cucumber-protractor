# CucumberJS with Gherkin scenarios in Protractor & Angular 4

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.0. (see below for standard cli commands).

An example project to setup CucumberJS with Gherkin scenarios in Protractor & Angular 4 As a base for the project I've used Sam Vloeberghs'
project https://github.com/samvloeberghs/protractor-gherkin-cucumberjs-angular

## Yarn
I have locked the packages with Yarn (https://yarnpkg.com/lang/en/) in an attempt to avoid npm installation issues.

## Getting started
Clone or download. cd into the root directory and run the yarn command (you will need to have the latest version of Yarn downloaded and installed).

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.



## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
